# WoopWoop #

This project is an intelliJ java project
It's a managed with git-flow

### What is this repository for? ###

* A bar POS
* Group 1 ITCOM project - "WoopWoop"

### How do I get set up? ###

* Install intellij
* Install SourceTree
* Clone repo with SourceTree
* Press the git flow button
* Switch to develop branch
* Start a new feature by pressing it again. 
* Install/Run PSQL
* Run the DBSchemaSQL commands
* Update the connection info in the DataSource object
* Start coding your new feature


### Contribution guidelines ###

* Before merging your feature into the develop branch, merge develop into your feature and make sure everything is working
* you might want to ask for someone to review your code if your'e not sure

### Who do I talk to? ###

* Repo owner or admin